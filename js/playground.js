class Playground{
    constructor(){
        this.fieldMap = [
            [
                [3, 1],
                [2, 0],
                [2, 3],
            ],
            [
                [1, 0],
                [0, 0],
                [0, 1],
            ],
            [
                [1, 0],
                [0, 0],
                [0, 0],
            ],
            [
                [3, 1],
                [2, 3],
                [0, 0],
            ],
            [
                [1, 0],
                [0, 0],
                [0, 0],
            ],
            [
                [1, 2],
                [0, 1],
                [0, 1],
            ],
        ],
        this.cells = [],
        this.units = []
    }

    /**
     * функция добавления клетки поля в матрицу клеток
     * @param {number} x - координата клетки в матрице
     * @param {number} y - координата клетки в матрице
     * @param {Cell} data - данные клетки
     */
    addCell(x, y, data){
        if (!this.cells[y]) this.cells[y] = []
        this.cells[y][x] = data
    }

    /**
     * функция поиска данных клетки в матрице клеток
     * возвращает экземпляр класса Cell, если клетка найдена 
     * и undefined - если нет
     * @param {number} x - координата клетки в матрице  
     * @param {number} y - координата клетки в матрице
     * @returns {Cell|undefined}
     */
    findCell(x, y){
        try{
            return this.cells[y][x]
        }
        catch{
            return undefined
        }
    }

    /**
     * функция вычисления X координаты спрайта объекта на полотне
     * @param {number} x - координата объекта в матрице
     * @param {number} y - координата объекта в матрице
     * @param {object} obj - экземпляр класса объекта, для которого вычисляется координата
     * @returns {number}
     */
    countXCoord(x, y, obj){
      return (51 * (x - y) - (obj.constructor.name === "Cell" ? 0 : 75))
    }

    /**
     * функция вычисления Y координаты спрайта объекта на полотне
     * @param {number} x - координата объекта в матрице
     * @param {number} y - координата объекта в матрице
     * @param {object} obj - экземпляр класса объекта, для которого вычисляется координата
     * @returns {number}
     */
    countYCoord(x, y, obj){
      return (38 * (x + y) - (obj.constructor.name === "Cell" ? 0 : 105))
    }

    /**
     * функция вычисляющая состояние стен клеток игрового поля
     */
    fillWallArrays(){
        this.cells.forEach(r => {
            r.forEach(c => {
                let targetCell = this.findCell(c.x + 1, c.y)
                c.walls.bottom_right.empty = (targetCell ? targetCell.walls.top_left.empty : false)
                targetCell = this.findCell(c.x, c.y + 1) 
                c.walls.bottom_left.empty = (targetCell ? targetCell.walls.top_right.empty : false)
            })
        })
    }

    /**
     * функция, раскладывающая hex значение цвета в массив [R,G,B]
     * @param {number} color - значение цвета в виде числа
     * @returns {Array[R,G,B]}
     */
    hexColorToRGB(color){
        return [ 
            parseInt(color.toString(16).substring(0,2), 16),
            parseInt(color.toString(16).substring(2,4), 16),
            parseInt(color.toString(16).substring(4,6), 16) 
        ]
    }

    /**
     * функция, собирающая hex значение цвета из массива [R,G,B]
     * @param {Array[R,G,B]} color - значение цвета в виде массива [R,G,B]
     * @returns {number}
     */
    rgbColorToHEX(color){
        return parseInt(color.map(x => x.toString(16)).join(''), 16)
    }

    /**
     * функция, вычисляющая новое значение цвета с заданным шагом
     * @param {Array[R,G,B]} color1 - текущее значение цвета в виде массива [R,G,B] 
     * @param {Array[R,G,B]} color2 - целевое значение цвета в виде массива [R,G,B] 
     * @param {number} step - шаг изменения цвета 
     * @returns {Array[R,G,B]}
     */
    mixRGBColors(color1, color2, step){
        const newColor = []
        for(let i = 0; i < 3; i++){
            if (color1[i] > color2[i]){
                newColor[i] = color1[i] - step
                newColor[i] = newColor[i] < color2[i] ? color2[i] : newColor[i]
            } 
            else {
                newColor[i] = color1[i] + step
                newColor[i] = newColor[i] > color2[i] ? color2[i] : newColor[i]
            }
        }
        return newColor
    }
}