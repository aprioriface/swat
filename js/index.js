    const textures = {}
    const animations = {} 
    
    let animationQueue = new Set()
    let app

    playground = new Playground()

    let type = "WebGL"
    if(!PIXI.utils.isWebGLSupported()){
      type = "canvas"
    }

    PIXI.utils.sayHello(type)

    function init(){
        //Create a Pixi Application
        app = new PIXI.Application({resizeTo: document.body, sortableChildren: true});
        //Add the canvas that Pixi automatically created for you to the HTML document
        document.body.appendChild(app.view);

        app.stage.x = window.innerWidth / 2
        app.stage.y = 150

        PIXI.Loader.shared
        .add([
            "assets/soldier_idle_bl.json",
            "assets/soldier_idle_tl.json",
            "assets/soldier_idle_tr.json",
            "assets/soldier_idle_br.json",
            "assets/soldier_sitidle.json",
            "assets/soldier_idletosit.json",
            "assets/soldier_rotate_1.json",
            "assets/soldier_shooting.json",
            "assets/soldier_walk.json",
            "assets/floor.png",
            "assets/walls.png",
            "assets/box.png"
        ])
        .load(cacheSprites);

        app.ticker.add(delta => gameLoop(delta));
    }

    //walk - .55
    //shoot - .25
    //sit - .15
    //idle - .25

    let tint = 0xAAAAAA
    let curSpriteIndex = 0
    let curFloorIndex = 0

    function cacheSprites(){
      textures.floor_texture = new PIXI.Texture(PIXI.Loader.shared.resources["assets/floor.png"].texture)
      textures.wall_texture_corner = new PIXI.Texture(PIXI.Loader.shared.resources["assets/walls.png"].texture)
      textures.wall_texture_corner.frame = new PIXI.Rectangle(160, 0, 80, 156)
      textures.wall_texture_left = new PIXI.Texture(PIXI.Loader.shared.resources["assets/walls.png"].texture)
      textures.wall_texture_left.frame = new PIXI.Rectangle(0, 0, 80, 156)
      textures.wall_texture_right = new PIXI.Texture(PIXI.Loader.shared.resources["assets/walls.png"].texture)
      textures.wall_texture_right.frame = new PIXI.Rectangle(80, 0, 80, 156)

      animations.soldier_idle_bl = PIXI.Loader.shared.resources["assets/soldier_idle_bl.json"].spritesheet.animations["swat_idle_v01"]
      animations.soldier_idle_tl = PIXI.Loader.shared.resources["assets/soldier_idle_tl.json"].spritesheet.animations["swat_idle_v03"]
      animations.soldier_idle_tr = PIXI.Loader.shared.resources["assets/soldier_idle_tr.json"].spritesheet.animations["swat_idle_v05"]
      animations.soldier_idle_br = PIXI.Loader.shared.resources["assets/soldier_idle_br.json"].spritesheet.animations["swat_idle_v07"]

      let curSpriteSheet = PIXI.Loader.shared.resources["assets/soldier_walk.json"].spritesheet
      animations.soldier_walk_bl = curSpriteSheet.animations["swat_walk_v01"]
      animations.soldier_walk_tl = curSpriteSheet.animations["swat_walk_v03"]
      animations.soldier_walk_tr = curSpriteSheet.animations["swat_walk_v05"]
      animations.soldier_walk_br = curSpriteSheet.animations["swat_walk_v07"]

      curSpriteSheet = PIXI.Loader.shared.resources["assets/soldier_sitidle.json"].spritesheet
      animations.soldier_sit_bl = curSpriteSheet.animations["swat_recharge_v01"]
      animations.soldier_sit_tl = curSpriteSheet.animations["swat_recharge_v03"]
      animations.soldier_sit_tr = curSpriteSheet.animations["swat_recharge_v05"]
      animations.soldier_sit_br = curSpriteSheet.animations["swat_recharge_v07"]

      curSpriteSheet = PIXI.Loader.shared.resources["assets/soldier_shooting.json"].spritesheet
      animations.soldier_shoot_bl = curSpriteSheet.animations["swat_shooting_v01"]
      animations.soldier_shoot_tl = curSpriteSheet.animations["swat_shooting_v03"]
      animations.soldier_shoot_tr = curSpriteSheet.animations["swat_shooting_v05"]
      animations.soldier_shoot_br = curSpriteSheet.animations["swat_shooting_v07"]

      curSpriteSheet = PIXI.Loader.shared.resources["assets/soldier_idletosit.json"].spritesheet
      animations.soldier_idle_to_sit_bl = curSpriteSheet.animations["swat_edging_v01"]
      animations.soldier_idle_to_sit_tl = curSpriteSheet.animations["swat_edging_v03"]
      animations.soldier_idle_to_sit_tr = curSpriteSheet.animations["swat_edging_v05"]
      animations.soldier_idle_to_sit_br = curSpriteSheet.animations["swat_edging_v07"]

      animations.soldier_sit_to_idle_bl = curSpriteSheet.animations["swat_edging_back_v01"]
      animations.soldier_sit_to_idle_tl = curSpriteSheet.animations["swat_edging_back_v03"]
      animations.soldier_sit_to_idle_tr = curSpriteSheet.animations["swat_edging_back_v05"]
      animations.soldier_sit_to_idle_br = curSpriteSheet.animations["swat_edging_back_v07"]

      curSpriteSheet = PIXI.Loader.shared.resources["assets/soldier_rotate_1.json"].spritesheet
      animations.soldier_idle_rotate_bl_to_br = curSpriteSheet.animations["swat_idle_rotate_bl_to_br"]
      animations.soldier_idle_rotate_bl_to_tl = curSpriteSheet.animations["swat_idle_rotate_bl_to_tl"]
      animations.soldier_idle_rotate_br_to_bl = curSpriteSheet.animations["swat_idle_rotate_br_to_bl"]
      animations.soldier_idle_rotate_br_to_tr = curSpriteSheet.animations["swat_idle_rotate_br_to_tr"]
      
      animations.soldier_idle_rotate_tr_to_tl = curSpriteSheet.animations["swat_idle_rotate_tr_to_tl"]
      animations.soldier_idle_rotate_tr_to_br = curSpriteSheet.animations["swat_idle_rotate_tr_to_br"]
      animations.soldier_idle_rotate_tl_to_bl = curSpriteSheet.animations["swat_idle_rotate_tl_to_bl"]
      animations.soldier_idle_rotate_tl_to_tr = curSpriteSheet.animations["swat_idle_rotate_tl_to_tr"]

      animations.soldier_idle_rotate_tl_to_br = curSpriteSheet.animations["swat_idle_rotate_tl_to_br"]
      animations.soldier_idle_rotate_tr_to_bl = curSpriteSheet.animations["swat_idle_rotate_tr_to_bl"]
      animations.soldier_idle_rotate_br_to_tl = curSpriteSheet.animations["swat_idle_rotate_br_to_tl"]
      animations.soldier_idle_rotate_bl_to_tr = curSpriteSheet.animations["swat_idle_rotate_bl_to_tr"]

      render()
    }

    function render(){
        playground.fieldMap.map((o, j) => o.map((x, i) => new Cell(i, j, x[0], x[1])))
        playground.fillWallArrays()

        new Unit(0, 0, "idle", "bl")
        //new Unit(0, 2, "idle", "br")

        app.stage.sortChildren()
    }

    function gameLoop(delta){
      for(let animationElement of animationQueue) animationElement.update(delta)
    }

    init()

    /**
     * класс клетки поля
     * 
     */
    class Cell{
      constructor(x, y, type, corners){
        this.x = x
        this.y = y

        this.highlighters = []
        this.tint = {
          value: 0xAAAAAA,
          step: 0
        }

        this.walls = {
          top_left: { empty: true, sprite : undefined, index: 0 },
          top_right: { empty: true, sprite : undefined, index: 1 },
          bottom_right: { empty: true, sprite : undefined, index: 2 },
          bottom_left: { empty: true, sprite : undefined, index: 3 },
        }
        this.corners = {
          top: { empty: true, sprite : undefined, index: 0 },
          right: { empty: true, sprite : undefined, index: 1 },
          bottom: { empty: true, sprite : undefined, index: 2 },
          left: { empty: true, sprite : undefined, index: 3 },
        }

        this.sprite = new PIXI.Sprite(textures.floor_texture)
        this.sprite.x = playground.countXCoord(x, y, this)
        this.sprite.y = playground.countYCoord(x, y, this)
        this.sprite.sortableChildren = true
        this.sprite.zIndex = (x + y) * 1000 - x * 10
        this.sprite.tint = this.tint.value

        if (corners == 1 || corners == 4) {
          this.corners.top.empty = false

          this.corners.top.sprite = new PIXI.Sprite(textures.wall_texture_corner)
          this.corners.top.sprite.x = 19
          this.corners.top.sprite.y = -111
          this.corners.top.sprite.width = 64
          this.corners.top.sprite.height = 125
          this.corners.top.sprite.tint = this.tint.value
          this.sprite.addChild(this.corners.top.sprite)

          if (playground.findCell(x-1, y-1)) this.corners.top.sprite.alpha = .6
        }

        if (corners == 2 || corners == 4) {
          this.corners.left.empty = false

          this.corners.left.sprite = new PIXI.Sprite(textures.wall_texture_corner)
          this.corners.left.sprite.x = -33
          this.corners.left.sprite.y = -72
          this.corners.left.sprite.width = 64
          this.corners.left.sprite.height = 125
          this.corners.left.sprite.tint = this.tint.value
          this.sprite.addChild(this.corners.left.sprite)

          if (playground.findCell(x-1, y) || playground.findCell(x-1, y-1)) this.corners.left.sprite.alpha = .6
        }

        if (corners == 3 || corners == 4) {
          this.corners.right.empty = false

          this.corners.right.sprite = new PIXI.Sprite(textures.wall_texture_corner)
          this.corners.right.sprite.x = 71
          this.corners.right.sprite.y = -72
          this.corners.right.sprite.width = 64
          this.corners.right.sprite.height = 125
          this.corners.right.sprite.tint = this.tint.value
          this.sprite.addChild(this.corners.right.sprite)

          if (playground.findCell(x, y-1) || playground.findCell(x-1, y-1)) this.corners.right.sprite.alpha = .6
        }



        if (type == 1 || type == 3) {
          this.walls.top_left.empty = false

          this.walls.top_left.sprite = new PIXI.Sprite(textures.wall_texture_left)
          this.walls.top_left.sprite.x = -13
          this.walls.top_left.sprite.y = -87
          this.walls.top_left.sprite.width = 64
          this.walls.top_left.sprite.height = 125
          this.walls.top_left.sprite.tint = this.tint.value
          this.sprite.addChild(this.walls.top_left.sprite)

          if (playground.findCell(x-1, y) || playground.findCell(x-1, y-1)) this.walls.top_left.sprite.alpha = .6
        }

        if (type == 2 || type == 3) {
          this.walls.top_right.empty = false

          this.walls.top_right.sprite = new PIXI.Sprite(textures.wall_texture_right)
          this.walls.top_right.sprite.x = 51
          this.walls.top_right.sprite.y = -87
          this.walls.top_right.sprite.width = 64
          this.walls.top_right.sprite.height = 125
          this.walls.top_right.sprite.tint = this.tint.value
          this.sprite.addChild(this.walls.top_right.sprite)

          if (playground.findCell(x, y-1) || playground.findCell(x-1, y-1)) this.walls.top_right.sprite.alpha = .6
        }

        this.highlight(false)

        app.stage.addChild(this.sprite)
        playground.addCell(x, y, this)
      }

      /**
       * функция подсветки клетки
       * @param {bool} state - true - для включения подсветки, false - для выключения
       * @param {object} initiator - объект-инициатор подсветки клетки
       * @param {number} speed - скорость изменения подсветки (опционально)
       */
      highlight(state, initiator, speed){
        if (state) this.highlighters.push(initiator)
        else this.highlighters = this.highlighters.filter(x => x !== initiator)

        const newTint = this.highlighters.length ? 0xFFFFFF : 0xAAAAAA
        if (this.tint.value !== newTint){
          this.tint.step = speed !== undefined ? speed : 2 
          this.tint.value = newTint
          animationQueue.add(this)
        } 
      }  

      update(delta){
        if (this.sprite.tint !== this.tint.value){
          let newTint

          if (this.tint.step === 0) newTint = this.tint.value
          else {
            const mixTintColor = playground.mixRGBColors(playground.hexColorToRGB(this.sprite.tint), playground.hexColorToRGB(this.tint.value), this.tint.step)

            newTint = playground.rgbColorToHEX(mixTintColor)            
          }

          this.sprite.tint = newTint
          for (let wall in this.walls){
            if (this.walls[wall].sprite) this.walls[wall].sprite.tint = newTint
          }
          for (let corner in this.corners){
            if (this.corners[corner].sprite) this.corners[corner].sprite.tint = newTint
          }
        }
        else animationQueue.delete(this)
      }
    }
  
    /**
     * класс игрового юнита
     */
    class Unit{
      constructor(x, y, state, direction, speed){
        this.state = state
        this.direction = direction
        this.x = x
        this.y = y

        this.visibility = {
          distance: 3,
          straight: false,
          cells: []
        }

        this.frames = new UnitAnimation
        this.texture = `soldier_${ state }_${ direction }`
        this.sprite = new PIXI.AnimatedSprite(animations[this.texture])
        
        this.sprite.x = playground.countXCoord(x, y, this)
        this.sprite.y = playground.countYCoord(x, y, this)
        this.sprite.animationSpeed = (speed ? speed : .25)
        this.sprite.scale.x = .5
        this.sprite.scale.y = .5
        this.sprite.zIndex = (this.x + this.y) * 1000 - (this.x - 1) * 10 + 1
        this.sprite.gotoAndPlay(Math.floor(Math.random() * this.sprite.textures.length))

        this.fillVisibleCells()
        this.highlightVisibleCells(true, 0)

        app.stage.addChild(this.sprite)
        playground.units.push(this)
      }

      /**
       * функция вычисления видимых клеток юнита
       * @param {number} x - координата клетки юнита (опционально)
       * @param {number} y - координата клетки юнита (опционально)
       * @param {string} direction - направление взгляда юнита (опционально)
       */
      fillVisibleCells(x, y, direction){
        x = x || this.x
        y = y || this.y
        direction = direction || this.direction

        const visibleCells = []
        let lastMainCell

        for(let i = 0; i <= this.visibility.distance; i++){
          let visibleX = x;
          let visibleY = y;
          let shiftX = 0
          let shiftY = 0
          let helpCells = []
          let problemWall = ""
          let testWalls = []

          switch(direction){
            case "bl":
              visibleY += i
              shiftX = 1
              problemWall = "top_right"
              helpCells = [
                [ { x: -1, y: 0 }, { x: -1, y: -1 } ],
                [ { x: 1, y: 0 }, { x: 1, y: -1 } ]
              ]
              testWalls = ["bottom_right", "top_left"]
              break
            case "br":
              visibleX += i
              shiftY = 1
              problemWall = "top_left"
              helpCells = [
                [ { x: 0, y: -1 }, { x: -1, y: -1 } ],
                [ { x: 0, y: 1 }, { x: -1, y: 1 } ]
              ]
              testWalls = ["bottom_left", "top_right"]
              break
            case "tr":
              visibleY -= i
              shiftX = 1
              problemWall = "bottom_left"
              helpCells = [
                [ { x: -1, y: 0 }, { x: -1, y: 1 } ],
                [ { x: 1, y: 0 }, { x: 1, y: 1 } ]
              ]
              testWalls = ["bottom_right", "top_left"]
              break
            case "tl":
              visibleX -= i
              shiftY = 1
              problemWall = "bottom_right"
              helpCells = [
                [ { x: 0, y: -1 }, { x: 1, y: -1 } ],
                [ { x: 0, y: 1 }, { x: 1, y: 1 } ]
              ]
              testWalls = ["bottom_left", "top_right", "bottom_right"]
              break
          }

          let helpCell1, helpCell2
          let targetCell = playground.findCell(visibleX, visibleY)
          if (i === 0 || targetCell && (targetCell.walls[problemWall].empty && visibleCells.find(x => x === lastMainCell))) visibleCells.push(targetCell)
          
          lastMainCell = targetCell

          if (!this.visibility.straight){
            for(let j = i; j > 0; j--){
              targetCell = playground.findCell(visibleX + shiftX * j, visibleY + shiftY * j)
              if (targetCell){
                helpCell1 = playground.findCell(targetCell.x + helpCells[0][0].x, targetCell.y + helpCells[0][0].y)
                helpCell2 = playground.findCell(targetCell.x + helpCells[0][1].x, targetCell.y + helpCells[0][1].y)
                
                if (j === i){
                  if (visibleCells.find(x => x === helpCell2) 
                    && !(!targetCell.walls[problemWall].empty && !helpCell1.walls[problemWall].empty)
                    && !(!helpCell1.walls[testWalls[0]].empty && !helpCell2.walls[testWalls[0]].empty)
                    && !(!helpCell2.walls[testWalls[0]].empty && !helpCell1.walls[problemWall].empty)
                    && !(!targetCell.walls[problemWall].empty && !targetCell.walls[testWalls[1]].empty))
                      visibleCells.push(targetCell)
                }
                else {
                  if (visibleCells.find(x => x === helpCell2)
                    && targetCell.walls[problemWall].empty
                    && helpCell2.walls[testWalls[0]].empty)
                      visibleCells.push(targetCell)
                }

                
              }

              targetCell = playground.findCell(visibleX - shiftX * j, visibleY - shiftY * j)
              if (targetCell){
                helpCell1 = playground.findCell(targetCell.x + helpCells[1][0].x, targetCell.y + helpCells[1][0].y)
                helpCell2 = playground.findCell(targetCell.x + helpCells[1][1].x, targetCell.y + helpCells[1][1].y)
                
                if (j === i){
                  if (visibleCells.find(x => x === helpCell2) 
                    && !(!targetCell.walls[problemWall].empty && !helpCell1.walls[problemWall].empty)
                    && !(!helpCell1.walls[testWalls[1]].empty && !helpCell2.walls[testWalls[1]].empty)
                    && !(!helpCell2.walls[testWalls[1]].empty && !helpCell1.walls[problemWall].empty)
                    && !(!targetCell.walls[problemWall].empty && !targetCell.walls[testWalls[0]].empty))
                    visibleCells.push(targetCell)
                }
                else{
                  if (visibleCells.find(x => x === helpCell2)
                    && targetCell.walls[problemWall].empty
                    && helpCell2.walls[testWalls[1]].empty)
                      visibleCells.push(targetCell)
                }
              } 
            }
          }
        }
        
        this.visibility.cells = visibleCells
      }

      /**
       * функция подсветки видимых клеток юнита
       * @param {bool} state - true - включает подсветку, false - выключает 
       * @param {number} speed - скорость изменения подсветки (опционально)
       */
      highlightVisibleCells(state, speed){
        this.visibility.cells.forEach(c => {
          c.highlight(state, this, speed)
        })
      }

      /**
       * функция, изменяющая техтуру спрайта
       * @param {string} state - тип анимации (idle, shoot, sit)
       * @param {string} direction - направление анимации (tl, tr, bl, br)
       * @param {number} speed - скорость анимации (опционально)
       * @param {bool} loop - флаг цикличности анимации (опционально)
       */
      resprite(state, direction, speed, loop){
        const newTexture = `soldier_${ state }_${ direction }`

        if (this.texture !== newTexture){
          this.sprite.textures = animations[newTexture]
          if (speed) this.sprite.animationSpeed = speed
          this.sprite.loop = loop ? true : false
          if (loop)
            this.sprite.gotoAndPlay(Math.floor(Math.random() * this.sprite.textures.length))
          else
            this.sprite.play()

          this.texture = newTexture
          this.state = state
          this.direction = direction
        }
      }

      /**
       * функция изменения состояния юнита
       * @param {Array} frames - массив состояний ({ state, x, y, speed, loop, moving })
       * @param {bool} restoreState - флаг возвращения к начальному состоянию (опционально)
       */
      animate(frames, restoreState){

        const curCell = { 
          x: this.x,
          y: this.y,
          direction: this.direction
        }

        this.frames.init(this)
        frames.map(f => {
          this.frames.steps.push(
            new UnitAnimationStep(
              f.state,
              f.x,
              f.y,
              curCell,
              f.speed,
              f.loop,
              f.moving,
              f.straight
            )
          )

          curCell.x = f.x
          curCell.y = f.y
          curCell.direction = this.frames.lastStep.direction
        })

        if (restoreState && this.frames.steps.length){
          this.frames.steps.push(
            new UnitAnimationStep(
              this.state,
              curCell.x,
              curCell.y,
              curCell,
              this.sprite.animationSpeed,
              this.sprite.loop,
              this.visibility.straight
            )
          )
        }

        this.frames.prepareToRender()
        animationQueue.add(this)
      }

      update(delta){
        let steps = this.frames.steps
        let counter = this.frames.counter
        let newStep = false

        if (counter < steps.length) {
          let curStep = steps[counter]
          if (curStep.moving !== false) {
            this.sprite.x = curStep.countNewCoord("x", this.sprite)
            this.sprite.y = curStep.countNewCoord("y", this.sprite)
            if (curStep.reachDistance(this.sprite)){
              this.x = curStep.moving.distanationCell.x
              this.y = curStep.moving.distanationCell.y
              newStep = true
            }
          }
          else newStep = true

          if (newStep){
            
            counter = ++this.frames.counter

            if (counter < steps.length){
              curStep = steps[counter]
              if (curStep.moving && (steps[counter - 1].direction === "br" || steps[counter - 1].direction === "bl") ) 
                this.sprite.zIndex = playground.findCell(curStep.moving.distanationCell.x, curStep.moving.distanationCell.y).sprite.zIndex + 1
              else 
                this.sprite.zIndex = (this.x + this.y) * 1000 - (this.x - 1) * 10 + 1
              this.visibility.straight = curStep.straight
              this.resprite(curStep.state, curStep.direction, curStep.speed, curStep.loop)
              app.stage.sortChildren()

              if (!curStep.moving && !curStep.loop){
                this.highlightVisibleCells(false)
                this.fillVisibleCells()
                this.highlightVisibleCells(true)
                animationQueue.delete(this)
                this.sprite.onComplete = () => { 
                  animationQueue.add(this) 
                }
              }
              else if (curStep.moving) {
                this.highlightVisibleCells(false)
                this.fillVisibleCells(curStep.moving.distanationCell.x, curStep.moving.distanationCell.y, curStep.direction)
                this.highlightVisibleCells(true)
              }
            }
          }
        }
        else {
          this.frames.clear()
          animationQueue.delete(this)
        }
      }
    }

    /**
     * класс массива анимаций юнита
     */
    class UnitAnimation{
      constructor(){
        this.counter = 0
        this.steps = []
      }

      get lastStep(){
        return this.steps[this.steps.length - 1]
      }

      /**
       * функция, создающая новый массив анимаций с начальным 
       * состоянием юнита, равным его текущему состоянию
       * @param {Unit} unit - данные начального состояния юнита
       */
      init(unit){
        this.clear()
        this.steps.push(
          new UnitAnimationStep(
            unit.state,
            unit.x,
            unit.y,
            { 
              x: unit.x,
              y: unit.y,
              direction: unit.direction
            },
            unit.sprite.animationSpeed,
            unit.sprite.loop,
            false,
            unit.visibility.straight
          )
        )
      }

      /**
       * функция очистки массива анимаций
       */
      clear(){
        this.counter = 0
        this.steps = []
      }

      /**
       * функция подготовки массива анимаций к прорисовки
       * вставляет кадры поворота юнита и приседания в те места,
       * где это необходимо
       */
      prepareToRender(){
        let lastStep = undefined
        for(let i = 0; i < this.steps.length; i++){
          const curStep = this.steps[i]
          if (lastStep) {

            if ((curStep.state !== "sit" && curStep.state !== "shoot") && (lastStep.state === "sit" || lastStep.state === "shoot")) {
              this.steps.splice(i++, 0, 
                new UnitAnimationStep().fill({
                  state: "sit_to_idle",
                  direction: lastStep.direction,
                  speed: .5,
                  straight: false,
                  moving: false,
                  loop: false
                })  
              )
            }

            if (curStep.direction != lastStep.direction){
              this.steps.splice(i++, 0,
                new UnitAnimationStep().fill({
                  state: `idle_rotate_${ lastStep.direction }_to`,
                  direction: curStep.direction,
                  speed: .25,
                  moving: false,
                  loop: false
                }) 
              )
            }

            if ((curStep.state === "sit" || curStep.state === "shoot") && (lastStep.state !== "sit" && lastStep.state !== "shoot")) {
              this.steps.splice(i++, 0, 
                  new UnitAnimationStep().fill({
                  state: "idle_to_sit",
                  direction: curStep.direction,
                  speed: .5,
                  straight: true,
                  moving: false,
                  loop: false
                })
              )
            }
          }
          lastStep = curStep
        }
      }
    }

    /**
     * класс кадра массива анимаций юнита
     */
    class UnitAnimationStep{
      constructor(state, x, y, curCell, speed, loop, moving, straight){
        if (state){
          this.state = state
          this.direction = this.getDirection(curCell, x, y)
          this.speed = speed ? speed : .25
          this.loop = loop || moving
          if (moving) {
            this.moving = {}
            this.moving.distanationCell = { x: x, y: y },
            this.moving.distanationCoords = { 
              x: playground.countXCoord(x, y, this),
              y: playground.countYCoord(x, y, this)
            },
            this.moving.steps = { 
              x: (this.moving.distanationCoords.x - playground.countXCoord(curCell.x, curCell.y, this)) / ((1 - this.speed) * 110),
              y: (this.moving.distanationCoords.y - playground.countYCoord(curCell.x, curCell.y, this)) / ((1 - this.speed) * 110) 
            }
          }
          else this.moving = false
          this.straight = straight ? true : false
        }
      }

      /**
       * функция, заполняющая кадр анимации предустановленными данными
       * @param {object} step - предустановленные данные
       * @returns {UnitAnimationStep}
       */
      fill(step){
        for (let prop in step){
          this[prop] = step[prop]
        }
        return this
      }

      /**
       * функция, возвращающая направление взгляда юнита в кадре
       * в зависимости от клетки текущего положения и клетки назначения
       * @param {object} curCell - данные клетки текущего положения (x, y, direction)
       * @param {number} x - координата клетки назначения
       * @param {number} y - координата клетки назначения
       * @returns {string} 
       */
      getDirection(curCell, x, y){
        if (curCell.x === x) 
          return (curCell.y === y ? curCell.direction : (curCell.y < y ? "bl" : "tr"))
        else if (curCell.x < x) 
          return (curCell.y === y ? "br" : (curCell.y < y ? "br" : "tr"))
        else 
          return (curCell.y === y ? "tl" : (curCell.y < y ? "bl" : "tl"))
      }

      /**
       * функция, вычисляющая координату следующего положения спрайта,
       * если текущий кадр анимации предполагает движение
       * @param {string} coord - имя координаты для вычисления (x, y)
       * @param {PIXI.Sprite} sprite - данные текущего спрайта юнита
       * @returns {number}  
       */
      countNewCoord(coord, sprite){
        let newCoord = sprite[coord]
        if (newCoord !== this.moving.distanationCoords[coord]){
          newCoord += this.moving.steps[coord]
          if ((newCoord - this.moving.distanationCoords[coord]) * this.moving.steps[coord] >= 0) newCoord = this.moving.distanationCoords[coord]
        }
        return newCoord
      }

      /**
       * функция, возвращающая true, если движущийся спрайт достиг конечной точки
       * @param {PIXI.Sprite} sprite - данные текущего спрайта юнита
       * @returns {bool} 
       */
      reachDistance(sprite){
        return (sprite.x === this.moving.distanationCoords.x && sprite.y === this.moving.distanationCoords.y)
      }
    }